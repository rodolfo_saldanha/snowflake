variable "app_name" {
  description = "The name of the App that will be launch. MUST BE ONLY ALPHA NUMERIC CHARACTERS BECAUSE OF DB IDENTIFIER"
}

variable "stage" {
  description = "Environment to launch"
}

variable "db1" {
  description = "Name of your dev database"
}

variable "db2" {
  description = "Name of your dev database"
}

variable "business_role1" {
  description = "Name of the business role"
}

variable "business_role2" {
  description = "Name of the business role"
}

variable "object_role1" {
  description = "Name of the business role"
}

variable "object_role2" {
  description = "Name of the business role"
}

variable "user1_name" {
  description = "User name"
}

variable "user1_login" {
  description = "User login"
}

variable "user2_name" {
  description = "User name"
}

variable "user2_login" {
  description = "User login"
}

variable "schema1" {
  description = "Schema name"
}

variable "schema2" {
  description = "Schema name"
}