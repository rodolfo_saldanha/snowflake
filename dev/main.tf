provider "aws" {
    version = "~> 2.70"
    region  = "us-east-2" 
}

provider "snowflake" {
  username = "rsaldanha"
  account  = "jk93470"
  role     = "ACCOUNTADMIN"
  region   = "us-east-2.aws"
}

terraform {
  required_version = ">= 0.13.5"

  backend "s3" {
    region = "us-east-2" 
    bucket = "tracking-api-snowflake-dev"
    key    = "tfstates/"
  }

  required_providers {
    snowflake = {
      source  = "chanzuckerberg/snowflake"
      version = "0.18.1"
    }
  }
}

module "db" {
  source         = "./modules/db"
  app_name       = var.app_name
  stage          = var.stage
  db1            = var.db1
  db2            = var.db2
  business_role1 = module.role.business_role1
  business_role2 = module.role.business_role2
}

module "schema" {
  source         = "./modules/schema"
  app_name       = var.app_name
  stage          = var.stage
  schema1        = var.schema1
  schema2        = var.schema2
  business_role1 = module.role.business_role1
  business_role2 = module.role.business_role2
  db1            = module.db.db1
  db2            = module.db.db2
}

module "role" {
  source         = "./modules/role"
  app_name       = var.app_name
  stage          = var.stage
  business_role1 = var.business_role1
  business_role2 = var.business_role2
  object_role1   = var.object_role1
  object_role2   = var.object_role2
}

module "user" {
  source         = "./modules/user"
  app_name       = var.app_name
  stage          = var.stage
  business_role1 = module.role.business_role1
  business_role2 = module.role.business_role2
  user1_name     = var.user1_name
  user1_login    = var.user1_login
  user2_name     = var.user2_name
  user2_login    = var.user2_login
}