# Simple name with only alpha characters
app_name       = "tracking-api"
# The stages can be anything you'd like though prod / dev / staging are common
stage          = "dev"
# Name of the dev database
db1 = "db1"
# Name of the dev database
db2 = "db2"
# Name of the business role
business_role1 = "analyst_basic"
# Name of the business role
business_role2 ="analyst_adv"
# Name of the object role
object_role1 = "db1_read_only"
# Name of the object role
object_role2 ="db2_read_only"
# User name
user1_name = "Rodolfo"
# User login
user1_login = "rodolfo@daredata.engineering"
# User name
user2_name = "Jose"
# User login
user2_login = "CHEESE_DATAENGINEER@MACANDCHEESE.COM"
# Schema name
schema1 = "schema1"
# Schema name
schema2 = "schema2"