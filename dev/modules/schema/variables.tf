variable "app_name" {
  description = "The name of the App that will be launch. MUST BE ONLY ALPHA NUMERIC CHARACTERS BECAUSE OF DB IDENTIFIER"
}

variable "stage" {
  description = "Environment to launch"
}

variable "business_role1" {
  description = "Name of the business role"
}

variable "business_role2" {
  description = "Name of the business role"
}

variable "db1" {
  description = "Name of the business role"
}

variable "db2" {
  description = "Name of the business role"
}

variable "schema1" {
  description = "Schema name"
}

variable "schema2" {
  description = "Schema name"
}