terraform {
  required_version = ">= 0.13"

  required_providers {
    snowflake = {
      source  = "chanzuckerberg/snowflake"
      version = "0.18.1"
    }
  }
}

resource "snowflake_schema" "schema1" {
  name     = var.schema1
  database = var.db1.name
  comment  = "Schema 1"
}

resource "snowflake_schema" "schema2" {
  name     = var.schema1
  database = var.db2.name
  comment  = "Schema 2"
}

resource "snowflake_schema_grant" "schema_grant_usage1" {
  schema_name   = snowflake_schema.schema1.name
  database_name = snowflake_schema.schema1.database
  privilege     = "USAGE"
  roles         = [var.business_role1.name]
}

resource "snowflake_schema_grant" "schema_grant_usage2" {
  schema_name   = snowflake_schema.schema2.name
  database_name = snowflake_schema.schema2.database
  privilege     = "USAGE"
  roles         = [var.business_role2.name]
}