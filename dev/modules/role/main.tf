terraform {
  required_version = ">= 0.13"

  required_providers {
    snowflake = {
      source  = "chanzuckerberg/snowflake"
      version = "0.18.1"
    }
  }
}

resource "snowflake_role" "business_role1" {
  name     = var.business_role1
  comment  = "Business role"
}

resource "snowflake_role" "business_role2" {
  name     = var.business_role2
  comment  = "Business role"
}

resource "snowflake_role_grants" "grant_business_role1" {
  role_name       = snowflake_role.business_role1.name
  roles           = [snowflake_role.business_role2.name]
}

resource "snowflake_role_grants" "grant_business_role2" {
  role_name       = snowflake_role.business_role2.name
  roles           = ["SYSADMIN"]
}

resource "snowflake_role" "obejct_role1" {
  name     = var.object_role1
  comment  = "Object role"
}

resource "snowflake_role" "obejct_role2" {
  name     = var.object_role2
  comment  = "Object role"
}

resource "snowflake_role_grants" "grant_object_role1" {
  role_name  = snowflake_role.obejct_role1.name
  roles      = [snowflake_role.business_role1.name]
}

resource "snowflake_role_grants" "grant_object_role2" {
  role_name       = snowflake_role.obejct_role2.name
  roles           = [snowflake_role.business_role2.name]
}

