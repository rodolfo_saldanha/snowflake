variable "app_name" {
  description = "The name of the App that will be launch. MUST BE ONLY ALPHA NUMERIC CHARACTERS BECAUSE OF DB IDENTIFIER"
}

variable "stage" {
  description = "Environment to launch"
}

variable "db1" {
  description = "Name of your dev database"
}

variable "db2" {
  description = "Name of your dev database"
}

variable "business_role1" {
  description = "Name of the business role"
}

variable "business_role2" {
  description = "Name of the business role"
}