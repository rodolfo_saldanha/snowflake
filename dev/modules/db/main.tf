terraform {
  required_providers {
    snowflake = {
      source  = "chanzuckerberg/snowflake"
      version = "0.18.1"
    }
  }
}

resource "snowflake_database" "db1" {
  name                        = var.db1
  comment                     = "Database 1"
}

resource "snowflake_database" "db2" {
  name                        = var.db2
  comment                     = "Database 2"
}

resource "snowflake_database_grant" "grant_role1" {
  database_name   = var.db1
  privilege       = "USAGE"
  roles           = [var.business_role1.name]
}

resource "snowflake_database_grant" "grant_role2" {
  database_name   = var.db2
  privilege       = "USAGE"
  roles           = [var.business_role2.name]
}