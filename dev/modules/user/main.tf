terraform {
  required_version = ">= 0.13"

  required_providers {
    snowflake = {
      source  = "chanzuckerberg/snowflake"
      version = "0.18.1"
    }
  }
}

resource "snowflake_user" "user1" {
  name                 = var.user1_name
  login_name           = var.user1_login
  default_role         = var.business_role1.name
  must_change_password = false
}

resource "snowflake_user" "user2" {
  name                 = var.user2_name
  login_name           = var.user2_login
  default_role         = var.business_role2.name
  must_change_password = false
}